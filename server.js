// init project with required resources
const express = require("express");
const bodyParser = require("body-parser");
const methodOverride = require("method-override");
const path = require("path");
const app = express();

// Instead of manually adding different things one by one, we use express.static option to save time.
app.use(express.static("public"));

//
app.use("/static", express.static("resources"));

// Body parsers and method overrides
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());
app.use(methodOverride());

// 500 Something Went Berserk
app.use(function(err, req, res, next) {
  console.error(err.stack);
  res
    .status(500)
    .send({
      error: 500,
      description:
        "Something went Berserk Mode, please contact Support or file an new issue on GitLab.",
      "gl-issue-tracker":
        "https://gitlab.com/MadeByThePinsTeam-DevLabs/movingtogitlab-guidebooks/issues/new"
    });
});

// 404 Page Not Found
app.use(function(req, res, next) {
  res
    .status(404)
    .send({
      error: 404,
      description:
        "The resource you're requested is not found. Maybe return to homepage?",
      homepage: "https://movingtogitlab-guidebooks.glitch.me"
    });
});

// listen for requests :)
const listener = app.listen(process.env.PORT, function() {
  console.log(
    "Successfully deployed! The #MovingToGitLab app is listening on port " +
      listener.address().port
  );
});

// If you have an Glitch.com mirror like this and worrying about its end of life every five mins.
// Simply configure PROJECT_DOMAIN variable into your project slug in envirnoment vars.
// This variable is configured automatically on Glitch.com projects.
process.on("SIGTERM", function() {
  console.log(
    "Resurrecting Glitch.com project " +
      process.env.PROJECT_DOMAIN +
      ", please wait..."
  );
  console.log("SIGTERM received, sending SOS to Resurrect...");
  require("https").get(
    "https://resurrect.glitch.me/" + process.env.PROJECT_DOMAIN + "/about",
    process.exit
  );
});
