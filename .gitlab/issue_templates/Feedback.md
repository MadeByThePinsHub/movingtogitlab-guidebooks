## Your Feedback Summary

(First, summarize your feedback here in less than 240 characters.)

## Detailed Information

(Then, write your feedback as detailed as possible. There is no limit when writing the whole story, just make sure it's not too short or long. Add more details like screenshots and some code snippets to better understand about your feedback.)

## Your Device Information
(These fields in this section are optional, because you're sending general feedback. Change these fields below based on your device. If not applicable, make it blank.)

**OS**: Windows 7

**Build Info**: Build 7601: SP1

**Browser**: Google Chrome

**Release Stream and Version**: Stable 79

**Bootstrap Compatibility Status**: supported

## Q and A
(Please answer the following questions as these are mandatory when you giving feedback about the project.)

### Question 1
Q: **Would do you like to recommend this Node.js app to someone? Tell us why.**

A:

### Question 2
Q: **From the rating of 0 to 10, please rate your experience about this project.**

A:

### Question 3
Q: **Based on your experience today and from your rating above, tell us how we can improve the app as whole.**

A:
