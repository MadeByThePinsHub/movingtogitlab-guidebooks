# Welcome to #MovingToGitLab project!

## About The Project
The #MovingToGitLab

## What's Needed?
If you want to contribute to this project locally or simply testing things out, you need:

- Bootstrap Docs
- GitLab Docs, as usual
- Express Docs, just an refresher
- Bootstrap Code Examples from w3schools.com for dummies
- Knowledge in coding HTML, CSS and Javascript
- Node.js and `npm` or `pnpm`, perferrly latest stable ones
- Bootstrap and Bootstrap Icons Node modules
- Express
- Any modern browser that supports Bootstrap 4.4.1+
- Prettier (recommended), to format and lint

## File Structure
The file structure in this project is compartible with GitLab Pages, as per GitLab Docs.

On the frontend,

- add and manage resources at `public/*` like `public/launchpad/index.html`;
  - for translations, look in `public/lang/*/index.html` where `./lang/*` is two-letter language code.
  - for resources such as images and CSS, use the `resources/*` direcotry.
    - it also contains templates for contributors to get started contributing. See `resources/templates/*`
- (only in Glitch.com) place images, audio and PDFs into the `assets` bucket; and
- store app secrets in `.env` or by using `export MYAPPVAR=secret` command.

On the backend,

- configure your Express server such as routes and requests on `server.js`; and
- manage frameworks and dependencies for this Node.js app in `package.json`

## Deployment Options
There are different ways to deploy this app in your own options. For contributing, simply fork this and code.

### For Glitch
The first option is easy to deploy, because you can see your changes instantly when deployed on Glitch.com.

1. Remix this project in Glitch. [![](https://cdn.glitch.com/2bdfb3f8-05ef-4035-a06e-2043962a3a13%2Fremix%402x.png?1513093958726)](https://glitch.com/edit/?utm_content=project_movingtogitlab-guidebooks&utm_source=remix_this&utm_medium=button&utm_campaign=glitchButton#!/remix/movingtogitlab-guidebooks)
2. It'll automatically deploys as you code unless **Refresh App on Changes** is disabled. To verify your deployment is working, look at the logs and find `Successfully deployed! The #MovingToGitLab app is listening on port F` message, where `F` is your project's port, and check HTML rendering.
3. To stop for an while, run `npm stop` command. To restart, simply code around or run `npm start`.

### Zeit Now

### Local Testing
1. Clone the whole project using `git clone`.
```bash
## Use the GitLab mirror instead
git clone https://gitlab.com
```